import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import  'rxjs/add/operator/map';


@Injectable()
export class SpotifyService {

  artistas:any[] = [];

  constructor(public http:HttpClient) { 
    console.log("servicioSpoty Iniciado");
  }

  getToken(){
    let token = "BQDJ_7GtRty186Q1LM7dCDpBlb80cKNDjcoBxHAYUr7WLeuhye8CmeTK--2ErIC7-3dtd9SJJvArCAGe5N8";
    return token;
  }

  
  getArtistas(termino:String){

      let token = this.getToken();

    let url = `https://api.spotify.com/v1/search?query=${ termino }&type=artist&limit=20`;

    let headersEnviado = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });

    return this.http.get(url, { headers:headersEnviado })
                    .map( (dataOut:any) => {
                      this.artistas = dataOut.artists.items
                      return this.artistas ;
                    });
            

  }


  getArtista( idArtista:string){
     let token = this.getToken();

    let url = `https://api.spotify.com/v1/artists/${ idArtista }`;

    let headersEnviado = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });

      return this.http.get(url, { headers:headersEnviado });


  }

  getTop( idArtista:string ){


     let token = this.getToken();

    let url = `https://api.spotify.com/v1/artists/${ idArtista }/top-tracks?country=ES`;

    let headersEnviado = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });

      return this.http.get(url, { headers:headersEnviado });


  }

}
