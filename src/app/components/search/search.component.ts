import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../serices/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  termino:string = "";

  constructor( public _spotify:SpotifyService ) { 
   
   
  }

  ngOnInit() {
  }

  buscarArtista(termino){
    if(this.termino.length == 0){
      return;
    }

    this._spotify.getArtistas(termino)
                .subscribe( dataOut => { });

  }

}
