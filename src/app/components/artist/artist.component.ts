import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../serices/spotify.service';


@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent implements OnInit {

  artistaEncontrado:any = {};
  topPistas:any = [];

  constructor( private activatedRoute: ActivatedRoute,
               public _spotifyService: SpotifyService
  ) { }

  ngOnInit() {

    this.activatedRoute.params
                       .map( dataOut => dataOut['id'])
                       .subscribe( id => {
                         console.log(id);
                         this._spotifyService.getArtista( id ).subscribe( artista => {
                          console.log(artista);
                          this.artistaEncontrado = artista;
                         });
                        
                        
                         this._spotifyService.getTop( id )
                                             .map ( ( dataOut:any ) => dataOut.tracks)
                                             .subscribe( pista => {
                           console.log("Tops Tracks del artista");
                           console.log(pista);
                           this.topPistas = pista;
                           console.warn("------")
                           console.log(this.topPistas);
                         })



                       });
    
  }

}
