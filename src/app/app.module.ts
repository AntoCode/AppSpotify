import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//para NgModel manejor de formularios
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

//moduleHttp

import { HttpClientModule } from '@angular/common/http';

//Rutas
import { app_routing } from './app.routes';

//Servicios

import { SpotifyService } from './serices/spotify.service';
import { SinFotoPipe } from './pipes/sin-foto.pipe';
import { ArtistComponent } from './components/artist/artist.component';
import { DonSeguroPipe } from './pipes/don-seguro.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NavbarComponent,
    SinFotoPipe,
    ArtistComponent,
    DonSeguroPipe
  ],
  imports: [
    BrowserModule,
    app_routing,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SpotifyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
